package com.example.demo.core;

import com.example.demo.utils.DriverManager;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.File;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class TestListener implements ITestListener {

    private static final Logger log = LogManager.getRootLogger();

    @Override
    public void onTestFailure(ITestResult result) {
        WebDriver driver = DriverManager.getDriver();
        if (driver != null) {
            log.info("Driver is not null. Proceeding to capture screenshot.");
            saveScreenshot(driver);
        } else {
            log.error("Driver is null. Cannot capture screenshot.");
        }
    }

    private void saveScreenshot(WebDriver driver) {
        File screenCapture = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(screenCapture, new File(
                    "target/screenshots/" + getCurrentTimeAsString() + ".png"));
            log.info("Screenshot saved successfully.");
        } catch (IOException e) {
            log.error("Failed to save screenshot: " + e.getMessage());
        }
    }

    private String getCurrentTimeAsString() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("uuuu-MM-dd_HH-mm-ss");
        return ZonedDateTime.now().format(formatter);
    }
}
