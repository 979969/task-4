package com.example.demo.tests;

import com.example.demo.pages.HomePage;
import com.example.demo.pages.PricingCalculatorPage;
import com.example.demo.pages.SearchResultsPage;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertTrue;

class MainTest extends BaseTest {


    @Test
    @DisplayName("Opening Calculator Page, Choosing Parameters, Sharing for summary and Comparing Prices")
    void Tests() {
        HomePage homePage = new HomePage(driver);
        homePage.open();
        SearchResultsPage searchResultsPage = homePage.searchForPricingCalculator("Google Cloud Platform Pricing Calculator");
        PricingCalculatorPage pricingCalculatorPage = searchResultsPage.goToPricingCalculatorPage();

        pricingCalculatorPage.clickAddToEstimate();
        pricingCalculatorPage.clickComputeEngine();
        pricingCalculatorPage.acceptCookies();
        pricingCalculatorPage.fillNumberOfInstances(4);
        pricingCalculatorPage.selectOperatingSystem("Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)");
        pricingCalculatorPage.selectMachineType("n1-standard-8");
        pricingCalculatorPage.clickCheckbox();
        pricingCalculatorPage.selectGpuModel("nvidia-tesla-t4"); // other option doesn't have Frankfurt location needed in next step
        pricingCalculatorPage.selectNumberOfGPU("1");
        pricingCalculatorPage.selectSSD("2x375 GB");
        pricingCalculatorPage.selectDatacenterLocation("europe-west3");
        pricingCalculatorPage.committedUse();
        double initialEstimatedCost = pricingCalculatorPage.retrieveTotalEstimatedCost();

        pricingCalculatorPage.clickShareButton();
        pricingCalculatorPage.clickOpenEstimateSummaryButton();
        double newEstimatedCost = pricingCalculatorPage.extractNewEstimatedCost();

        assertTrue(initialEstimatedCost == newEstimatedCost, "Estimated costs are not matching");
    }

}

