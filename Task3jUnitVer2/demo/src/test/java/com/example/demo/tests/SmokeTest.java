package com.example.demo.tests;

import com.example.demo.pages.HomePage;
import com.example.demo.pages.PricingCalculatorPage;
import com.example.demo.pages.SearchResultsPage;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertTrue;

class SmokeTest extends BaseTest {


    @Test
    @DisplayName("Opening Calculator Page, Sharing for summary")
    void Tests() {
        HomePage homePage = new HomePage(driver);
        homePage.open();
        SearchResultsPage searchResultsPage = homePage.searchForPricingCalculator("Google Cloud Platform Pricing Calculator");
        PricingCalculatorPage pricingCalculatorPage = searchResultsPage.goToPricingCalculatorPage();

        pricingCalculatorPage.clickAddToEstimate();
        pricingCalculatorPage.clickComputeEngine();
        pricingCalculatorPage.acceptCookies();

        pricingCalculatorPage.clickShareButton();
        pricingCalculatorPage.clickOpenEstimateSummaryButton();
        double newEstimatedCost = pricingCalculatorPage.extractNewEstimatedCost();

        Assertions.assertNotNull(newEstimatedCost, "New estimated cost is null");

    }

}

