package com.example.demo.tests;

import com.example.demo.pages.HomePage;
import com.example.demo.pages.PricingCalculatorPage;
import com.example.demo.pages.SearchResultsPage;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertTrue;

class SearchAndOpenCalculatorTest extends BaseTest {


    @Test
    @DisplayName("Opening Home Page, Searching and Opening Calculator Page")
    void Tests() {
        HomePage homePage = new HomePage(driver);
        homePage.open();
        SearchResultsPage searchResultsPage = homePage.searchForPricingCalculator("Google Cloud Platform Pricing Calculator");
        PricingCalculatorPage pricingCalculatorPage = searchResultsPage.goToPricingCalculatorPage();
        double initialEstimatedCost = pricingCalculatorPage.retrieveTotalEstimatedCost();

        Assertions.assertNotNull(initialEstimatedCost, "Calculator search and opening failed");
    }

}

