package com.example.demo.utils;

import org.openqa.selenium.WebDriver;

import java.util.Set;

public class TabSwitcher {
    public static void switchToNewTab(WebDriver driver) {
        String currentHandle = driver.getWindowHandle();
        Set<String> handles = driver.getWindowHandles();
        for (String handle : handles) {
            if (!handle.equals(currentHandle)) {
                driver.switchTo().window(handle);
                break;
            }
        }
    }
}
