package com.example.demo.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AcceptCookies {

    public static void acceptCookies(WebDriver driver, WebElement acceptCookiesButton) {
        try {
            acceptCookiesButton.click();
        } catch (Exception e) {
            System.out.println("Cookie notification bar not found or already accepted.");
        }
    }
}
