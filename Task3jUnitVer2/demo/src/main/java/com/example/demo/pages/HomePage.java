package com.example.demo.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

    @FindBy(css = "div.ND91id")
    private WebElement searchIcon;

    @FindBy(name = "q")
    private WebElement searchField;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        driver.get("https://cloud.google.com/");
    }

    public SearchResultsPage searchForPricingCalculator(String query) {
        searchIcon.click();
        searchField.sendKeys(query);
        searchField.submit();
        return new SearchResultsPage(driver);
    }
}
